package de.hsbremen.dlx.assembler;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main parsing logic of DLXAssembler. Constructor from line creates Opcode and
 * sets necessary fields like registers, immediate, etc.
 * 
 * @author bschaefer
 * @version 21.02.2016
 * 
 */
public class Instruction {

	private static Pattern instructionPattern = Pattern.compile("^\\s*\\w+");
	private static final String registerReplace = "[\\srR,]";
	private static final String immediateReplace = "[\\s,]";

	private Opcode opcode;
	private byte rs1;
	private byte rs2;
	private byte rd;
	private short immediate;
	private int pos;
	private String label;
	private int labelPos;
	private String line;

	public Instruction(String line, int position) throws AssemblyException {
		pos = position;
		this.line = line;
		line = line.trim();
		Matcher matcher = instructionPattern.matcher(line);
		int firstSemicolon = line.indexOf(';');
		String tokens[] = null;
		Functioncode fcode = null;

		matcher.lookingAt();
		String command = line.substring(matcher.start(), matcher.end());
		if (firstSemicolon != -1) {
			tokens = line.substring(matcher.end(), firstSemicolon).split(",");
		} else {
			tokens = line.substring(matcher.end()).split(",");
		}
		if (tokens.length <= 0) {
			throw new AssemblyException();
		}
		opcode = new Opcode(command);

		switch (opcode.getType()) {
		case IMMEDIATE:
			switch (opcode.getValue()) {
			case Opcode.BEQZ:
			case Opcode.BNEZ:
				if (!(tokens.length >= 2)) {
					throw new AssemblyException();
				}
				rs1 = getRegister(tokens[0]);
				label = tokens[1].trim();
				break;
			case Opcode.LW:
				rd = getRegister(tokens[0]);
				getRegisterOffset(tokens[1]);
				break;
			case Opcode.SW:
				getRegisterOffset(tokens[0]);
				rd = getRegister(tokens[1]);
				break;
			default:
				if (!(tokens.length >= 3)) {
					throw new AssemblyException();
				}
				rd = getRegister(tokens[0]);
				rs1 = getRegister(tokens[1]);
				if (tokens[2].matches("\\s*\\d+\\s*"))
					immediate = new Short(tokens[2].replaceAll(
							immediateReplace, ""));
				else
					label = tokens[2].trim();
				break;
			}
			break;
		case JUMP:
			if (tokens.length < 1)
				throw new AssemblyException();
			label = tokens[0].trim();
			break;
		case R_FP:
		case R_INT:
			fcode = opcode.getFunctioncode();
			if (fcode != null
					&& (fcode.getValue() == Functioncode.MOVI2FP || fcode
							.getValue() == Functioncode.MOVFP2I)) {

				if (!(tokens.length >= 2)) {
					throw new AssemblyException();
				}
				rd = getRegister(tokens[0]);
				rs1 = getRegister(tokens[1]);
				rs2 = 0;
			} else {
				if (!(tokens.length >= 3)) {
					throw new AssemblyException();
				}
				rd = getRegister(tokens[0]);
				rs1 = getRegister(tokens[1]);
				rs2 = getRegister(tokens[2]);
			}
			break;
		default:
			break;

		}
	}

	private void getRegisterOffset(String token) {
		int openParen = token.indexOf('(');
		int closeParen = token.indexOf(')');
		rs1 = getRegister(token.substring(openParen + 1, closeParen));
		immediate = new Short(token.substring(0, openParen).trim());
		if (immediate % 4 != 0)
			System.out
					.println("Warning! Offset is not divisible by 4\n" + line);
	}

	private Byte getRegister(String register) {
		return new Byte(register.replaceAll(registerReplace, ""));
	}

	public int getValue() {
		int value = opcode.getValue() << 26;
		switch (opcode.getType()) {
		case IMMEDIATE:
			if (opcode.getValue() == Opcode.BEQZ
					|| opcode.getValue() == Opcode.BNEZ) {
				value |= (rs1 << 21) | ((labelPos - pos - 1) & 0x0000FFFF);
			} else {
				value |= (rs1 << 21) | (rd << 16) | (immediate & 0x0000FFFF);
			}
			break;
		case JUMP:
			value |= labelPos & 0x03FFFFFF;
			break;
		case R_FP:
			value |= (rs1 << 21) | (rs2 << 16) | (rd << 11)
					| (opcode.getFPFunctioncode().getValue() & 0x1F);
			break;
		case R_INT:
			value |= (rs1 << 21) | (rs2 << 16) | (rd << 11)
					| (opcode.getFunctioncode().getValue() & 0x3F);
			break;
		}
		return value;
	}

	public int getPosition() {
		return pos;
	}

	public void resolveLabel(Map<Integer, Label> labels)
			throws AssemblyException {
		Label lab = null;
		if (label != null) {
			lab = labels.get(label.hashCode());
			if (lab == null)
				throw new AssemblyException(label);
			if (opcode.getType() == InstructionType.IMMEDIATE
					&& opcode.getValue() != Opcode.BEQZ
					&& opcode.getValue() != Opcode.BNEZ)
				/* times four because it can only reasonably be used in lw/sw */
				immediate = (short) (lab.getInstruction() * 4);
			else
				labelPos = lab.getInstruction();

		}
	}

	@Override
	public String toString() {
		return String.format(";%s: %s\n",
				String.format("%4d", pos).replace(' ', '0'), line)
				+ String.format("%8h", getValue()).replace(' ', '0');
	}
}
