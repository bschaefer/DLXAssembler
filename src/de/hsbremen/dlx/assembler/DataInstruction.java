package de.hsbremen.dlx.assembler;

/**
 * Parser of control sequences to write data into memory
 * 
 * @author bschaefer
 * @version 19.02.2016
 * 
 */
public class DataInstruction {

	private int value;
	private String line;
	private int pos;

	public DataInstruction(String line, int position) throws AssemblyException {
		this.line = line;
		pos = position;
		if (line.contains(".word")) {
			value = new Integer(line.replace(".word", "").trim());
		} else {
			throw new AssemblyException("Invalid control sequence");
		}
	}

	@Override
	public String toString() {
		return String.format(";%s: %s\n%s",
				String.format("%4d", pos).replace(' ', '0'), line, String
						.format("%8h", value).replace(' ', '0'));
	}

}
