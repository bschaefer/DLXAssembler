package de.hsbremen.dlx.assembler;

/**
 * Functioncode constants and constructor from functioncode-literal as String
 * 
 * @author bschaefer
 * @version 17.12.2015
 * 
 */
public class FloatingPointFunctioncode {
	public static final byte ADDF = 0;
	public static final byte SUBF = 1;
	public static final byte MULTF = 2;
	public static final byte DIVF = 3;
	public static final byte ADDD = 4;
	public static final byte SUBD = 5;
	public static final byte MULTD = 6;
	public static final byte DIVD = 7;
	public static final byte CVTF2D = 8;
	public static final byte CVTF2I = 9;
	public static final byte CVTD2F = 10;
	public static final byte CVTD2I = 11;
	public static final byte CVTI2F = 12;
	public static final byte CVTI2D = 13;
	public static final byte MULT = 14;
	public static final byte DIV = 15;
	public static final byte EQF = 16;
	public static final byte NEF = 17;
	public static final byte LTF = 18;
	public static final byte GTF = 19;
	public static final byte LEF = 20;
	public static final byte GEF = 21;
	public static final byte MULTU = 22;
	public static final byte DIVU = 23;
	public static final byte EQD = 24;
	public static final byte NED = 25;
	public static final byte LTD = 26;
	public static final byte GTD = 27;
	public static final byte LED = 28;
	public static final byte GED = 29;

	private byte value;

	public FloatingPointFunctioncode(String fcode) throws AssemblyException {
		fcode = fcode.toUpperCase();
		switch (fcode) {
		case "ADDF":
			value = ADDF;
			notImplemented(fcode);
			break;
		case "SUBF":
			value = SUBF;
			notImplemented(fcode);
			break;
		case "MULTF":
			value = MULTF;
			notImplemented(fcode);
			break;
		case "DIVF":
			value = DIVF;
			notImplemented(fcode);
			break;
		case "ADDD":
			value = ADDD;
			notImplemented(fcode);
			break;
		case "SUBD":
			value = SUBD;
			notImplemented(fcode);
			break;
		case "MULTD":
			value = MULTD;
			notImplemented(fcode);
			break;
		case "DIVD":
			value = DIVD;
			notImplemented(fcode);
			break;
		case "CVTF2D":
			value = CVTF2D;
			notImplemented(fcode);
			break;
		case "CVTF2I":
			value = CVTF2I;
			notImplemented(fcode);
			break;
		case "CVTD2F":
			value = CVTD2F;
			notImplemented(fcode);
			break;
		case "CVTD2I":
			value = CVTD2I;
			notImplemented(fcode);
			break;
		case "CVTI2F":
			value = CVTI2F;
			notImplemented(fcode);
			break;
		case "CVTI2D":
			value = CVTI2D;
			notImplemented(fcode);
			break;
		case "MULT":
			value = MULT;
			break;
		case "DIV":
			value = DIV;
			notImplemented(fcode);
			break;
		case "EQF":
			value = EQF;
			notImplemented(fcode);
			break;
		case "NEF":
			value = NEF;
			notImplemented(fcode);
			break;
		case "LTF":
			value = LTF;
			notImplemented(fcode);
			break;
		case "GTF":
			value = GTF;
			notImplemented(fcode);
			break;
		case "LEF":
			value = LEF;
			notImplemented(fcode);
			break;
		case "GEF":
			value = GEF;
			notImplemented(fcode);
			break;
		case "MULTU":
			value = MULTU;
			break;
		case "DIVU":
			value = DIVU;
			notImplemented(fcode);
			break;
		case "EQD":
			value = EQD;
			notImplemented(fcode);
			break;
		case "NED":
			value = NED;
			notImplemented(fcode);
			break;
		case "LTD":
			value = LTD;
			notImplemented(fcode);
			break;
		case "GTD":
			value = GTD;
			notImplemented(fcode);
			break;
		case "LED":
			value = LED;
			notImplemented(fcode);
			break;
		case "GED":
			value = GED;
			notImplemented(fcode);
			break;
		default:
			throw new AssemblyException(fcode);
		}
	}

	public byte getValue() {
		return value;
	}

	private void notImplemented(String fcode) {
		System.err.println("Instruction: " + fcode
				+ " is not implemented in DLX_MULTI_CYCLE!");
	}

}
