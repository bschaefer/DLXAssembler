package de.hsbremen.dlx.assembler;

/**
 * Functioncode constants and constructor from functioncode-literal as String
 * 
 * @author bschaefer
 * @version 19.02.2016
 * 
 */
public class Functioncode {
	public static final byte SLL = 4;
	public static final byte SRL = 6;
	public static final byte SRA = 7;
	public static final byte TRAP = 12;
	public static final byte ADD = 32;
	public static final byte ADDU = 33;
	public static final byte SUB = 34;
	public static final byte SUBU = 35;
	public static final byte AND = 36;
	public static final byte OR = 37;
	public static final byte XOR = 38;
	public static final byte SEQ = 40;
	public static final byte SNE = 41;
	public static final byte SLT = 42;
	public static final byte SGT = 43;
	public static final byte SLE = 44;
	public static final byte SGE = 45;
	public static final byte MOVI2S = 48;
	public static final byte MOVS2I = 49;
	public static final byte MOVF = 50;
	public static final byte MOVD = 51;
	public static final byte MOVFP2I = 52;
	public static final byte MOVI2FP = 53;

	private byte value;

	public Functioncode(String fcode) throws AssemblyException {
		fcode = fcode.toUpperCase();
		switch (fcode) {
		case "SLL":
			value = SLL;
			break;
		case "SRL":
			value = SRL;
			break;
		case "SRA":
			value = SRA;
			break;
		case "TRAP": // TODO same as opcode?
			value = TRAP;
			notImplemented(fcode);
			break;
		case "ADD":
			value = ADD;
			break;
		case "ADDU":
			value = ADDU;
			notImplemented(fcode);
			break;
		case "SUB":
			value = SUB;
			break;
		case "SUBU":
			value = SUBU;
			notImplemented(fcode);
			break;
		case "AND":
			value = AND;
			break;
		case "OR":
			value = OR;
			break;
		case "XOR":
			value = XOR;
			break;
		case "SEQ":
			value = SEQ;
			notImplemented(fcode);
			break;
		case "SNE":
			value = SNE;
			notImplemented(fcode);
			break;
		case "SLT":
			value = SLT;
			break;
		case "SGT":
			value = SGT;
			notImplemented(fcode);
			break;
		case "SLE":
			value = SLE;
			notImplemented(fcode);
			break;
		case "SGE":
			value = SGE;
			notImplemented(fcode);
			break;
		case "MOVI2S":
			value = MOVI2S;
			notImplemented(fcode);
			break;
		case "MOVS2I":
			value = MOVS2I;
			notImplemented(fcode);
			break;
		case "MOVF":
			value = MOVF;
			notImplemented(fcode);
			break;
		case "MOVD":
			value = MOVD;
			notImplemented(fcode);
			break;
		case "MOVFP2I":
			value = MOVFP2I;
			break;
		case "MOVI2FP":
			value = MOVI2FP;
			break;
		default:
			throw new AssemblyException(fcode);
		}
	}

	public byte getValue() {
		return value;
	}

	private void notImplemented(String fcode) {
		System.err.println("Instruction: " + fcode
				+ " is not implemented in DLX_MULTI_CYCLE!");
	}

}
