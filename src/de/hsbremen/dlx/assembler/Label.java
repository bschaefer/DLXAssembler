package de.hsbremen.dlx.assembler;

/**
 * Simple label with a name and the number of the following Instruction.
 * 
 * @author bschaefer
 * @version 10.12.2015
 * 
 */
public class Label {
	private String name;
	private String line;
	private int instruction;

	public Label(String line, int followingInstruction)
			throws AssemblyException {
		instruction = followingInstruction;
		this.line = line;
		String parts[] = line.split(":");
		if (parts.length < 1)
			throw new AssemblyException(line);
		name = parts[0].trim();
	}

	public int getInstruction() {
		return instruction;
	}

	@Override
	public String toString() {
		return ";" + line;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
