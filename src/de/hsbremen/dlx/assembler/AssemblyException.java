package de.hsbremen.dlx.assembler;

import java.text.ParseException;

/**
 * Exception of the DLXAssembler.
 * 
 * @author bschaefer
 * @version 10.12.2015
 * 
 */
public class AssemblyException extends ParseException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5613188486743769069L;

	public AssemblyException(){
		super("",0);
	}
	
	public AssemblyException(String token){
		super(token,0);
	}

}
