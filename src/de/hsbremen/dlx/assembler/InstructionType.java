package de.hsbremen.dlx.assembler;

enum InstructionType {
	JUMP, IMMEDIATE, R_INT, R_FP
}
