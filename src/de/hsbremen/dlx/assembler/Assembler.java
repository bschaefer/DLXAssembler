package de.hsbremen.dlx.assembler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main class of DLXAssembler. Reads file, judges if comment, label or
 * instruction and calls corresponding constructor.
 * 
 * @author bschaefer
 * @version 19.02.2016
 * 
 */
public class Assembler {

	enum AssemblerState {
		DATA, CODE
	}

	private static Pattern commentLine = Pattern.compile("^\\s*;.*$");
	private static Pattern labelLine = Pattern.compile("^[^;]*:[^\\.]*$");
	private static Pattern blankLine = Pattern.compile("^\\s*$");
	private static Pattern controlLine = Pattern
			.compile("^\\s*([^;]*:)?\\s*(\\..*)$");

	private ArrayList<Object> instructions;
	private HashMap<Integer, Label> labels;
	private AssemblerState state;
	private int instructionCount = 0;
	private int lineCount = 0;

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out
					.printf("Usage is: java -jar DLXAssembler.jar inputFile outputFile");
			System.exit(1);
		}
		Assembler asm = new Assembler();
		asm.parseFile(FileSystems.getDefault().getPath(args[0]), FileSystems
				.getDefault().getPath(args[1]));

	}

	public Assembler() {
		instructions = new ArrayList<Object>();
		labels = new HashMap<Integer, Label>();
		state = AssemblerState.CODE;
	}

	public void parseFile(Path input, Path output) {
		Charset charset = StandardCharsets.UTF_8;
		Instruction inst = null;

		try (BufferedReader reader = Files.newBufferedReader(input, charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				++lineCount;
				switch (state) {
				case CODE:
					parseCodeLine(line);
					break;
				case DATA:
					parseDataLine(line);
					break;
				}

			}
		} catch (IOException x) {
			System.err.format(
					"An unexpected Error occurred\nIOException: %s%n", x);
			System.exit(-1);
		}

		for (Object obj : instructions) {
			if (obj instanceof Instruction) {
				inst = (Instruction) obj;
				try {
					inst.resolveLabel(labels);
				} catch (AssemblyException e) {
					System.err.println("Could not find label: "
							+ e.getLocalizedMessage());
					System.exit(1);
				}
			}
		}

		try (BufferedWriter writer = Files.newBufferedWriter(output, charset)) {
			for (Object obj : instructions) {
				writer.write(obj.toString(), 0, obj.toString().length());
				writer.newLine();
			}
		} catch (IOException x) {
			System.err.format(
					"An unexpected Error occurred\nIOException: %s%n", x);
			System.exit(-1);
		}

	}

	private void parseCodeLine(String line) {
		Instruction inst = null;
		if (commentLine.matcher(line).matches()
				|| blankLine.matcher(line).matches()) {
			instructions.add(line);
			return;
		}
		if (addLabelLine(line))
			return;
			
		if (controlLine.matcher(line).matches()) {
			if (line.contains(".data")) {
				state = AssemblerState.DATA;
			} else if (line.contains(".text")) {
				state = AssemblerState.CODE;
			} else {
				printWarning("Invalid control statement " + line + " in scope "
						+ state.name());
			}
			return;
		}
		try {
			inst = new Instruction(line, instructionCount++);
			instructions.add(inst);
		} catch (AssemblyException e) {
			printError("Invalid Instruction Syntax: " + line);
		} catch (Exception e) {
			e.printStackTrace();
			printError("An unexpected Error occurred: " + line);
		}
	}

	private void parseDataLine(String line) {
		DataInstruction data = null;
		Matcher lineMatcher = controlLine.matcher(line);
		Label label = null;
		if(addLabelLine(line)){
			return;
		}
		if (lineMatcher.matches()) {
			if (line.contains(".data")) {
				state = AssemblerState.DATA;
			} else if (line.contains(".text")) {
				state = AssemblerState.CODE;
			} else {
				if(lineMatcher.start(1) != -1){
					try {
						label = new Label(lineMatcher.group(1), instructionCount);
						labels.put(label.hashCode(), label);
						instructions.add(label);
					} catch (AssemblyException e) {
						printError("Invalid Label Syntax: " + line);
					}
				}
				try {
					data = new DataInstruction(lineMatcher.group(2), instructionCount++);
					instructions.add(data);
				} catch (AssemblyException e) {
					printWarning("Invalid control statement " + line
							+ " in scope " + state.name());
				}
			}
			return;
		}
	}

	public void printError(String message) {
		System.err.println("Error on line: " + lineCount + "\n" + message);
		System.exit(-1);
	}

	public void printWarning(String message) {
		System.err.println("Warning on line: " + lineCount + "\n" + message);
	}
	
	private boolean addLabelLine(String line){
		Label label = null;
		if (labelLine.matcher(line).matches()) {
			try {
				label = new Label(line, instructionCount);
				labels.put(label.hashCode(), label);
				instructions.add(label);
			} catch (AssemblyException e) {
				printError("Invalid Label Syntax: " + line);
			}
			return true;
		}
		return false;
	}

}
