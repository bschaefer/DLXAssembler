package de.hsbremen.dlx.assembler;

/**
 * Opcode constants and constructor from opcode-literal. Calls Functioncode
 * constructor if unknown.
 * 
 * @author bschaefer
 * @version 17.12.2015
 * 
 */
public class Opcode {
	public static final byte SPECIAL = 0;
	public static final byte FPARITH = 1;
	public static final byte J = 2;
	public static final byte JAL = 3;
	public static final byte BEQZ = 4;
	public static final byte BNEZ = 5;
	public static final byte BFPT = 6;
	public static final byte BFPF = 7;
	public static final byte ADDI = 8;
	public static final byte ADDUI = 9;
	public static final byte SUBI = 10;
	public static final byte SUBUI = 11;
	public static final byte ANDI = 12;
	public static final byte ORI = 13;
	public static final byte XORI = 14;
	public static final byte LHI = 15;
	public static final byte RFE = 16;
	public static final byte TRAP = 17;
	public static final byte JR = 18;
	public static final byte JALR = 19;
	public static final byte SLLI = 20;
	public static final byte SRLI = 22;
	public static final byte SRAI = 23;
	public static final byte SEQI = 24;
	public static final byte SNEI = 25;
	public static final byte SLTI = 26;
	public static final byte SGTI = 27;
	public static final byte SLEI = 28;
	public static final byte SGEI = 29;
	public static final byte LB = 32;
	public static final byte LH = 33;
	public static final byte LW = 35;
	public static final byte LBU = 36;
	public static final byte LHU = 37;
	public static final byte LF = 38;
	public static final byte LD = 39;
	public static final byte SB = 40;
	public static final byte SH = 41;
	public static final byte SW = 43;
	public static final byte SF = 46;
	public static final byte SD = 47;

	private byte value;
	private Functioncode fcode;
	private InstructionType type;
	private FloatingPointFunctioncode fpFcode;

	public Opcode(String opcode) throws AssemblyException {
		opcode = opcode.toUpperCase();
		switch (opcode) {
		case "SPECIAL":
			// see default
			break;
		case "FPARITH": // TODO
			value = FPARITH;
			type = InstructionType.R_FP;
			notImplemented(opcode);
			break;
		case "J":
			value = J;
			type = InstructionType.JUMP;
			break;
		case "JAL":
			value = JAL;
			type = InstructionType.JUMP;
			notImplemented(opcode);
			break;
		case "BEQZ":
			value = BEQZ;
			type = InstructionType.IMMEDIATE;
			break;
		case "BNEZ":
			value = BNEZ;
			type = InstructionType.IMMEDIATE;
			break;
		case "BFPT":
			value = BFPT;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "BFPF":
			value = BFPF;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "ADDI":
			value = ADDI;
			type = InstructionType.IMMEDIATE;
			break;
		case "ADDUI":
			value = ADDUI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SUBI":
			value = SUBI;
			type = InstructionType.IMMEDIATE;
			break;
		case "SUBUI":
			value = SUBUI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "ANDI":
			value = ANDI;
			type = InstructionType.IMMEDIATE;
			break;
		case "ORI":
			value = ORI;
			type = InstructionType.IMMEDIATE;
			break;
		case "XORI":
			value = XORI;
			type = InstructionType.IMMEDIATE;
			break;
		case "LHI":
			value = LHI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "RFE":
			value = RFE;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "TRAP":
			value = TRAP;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "JR":
			value = JR;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "JALR":
			value = JALR;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SLLI":
			value = SLLI;
			type = InstructionType.IMMEDIATE;
			break;
		case "SRLI":
			value = SRLI;
			type = InstructionType.IMMEDIATE;
			break;
		case "SRAI":
			value = SRAI;
			type = InstructionType.IMMEDIATE;
			break;
		case "SEQI":
			value = SEQI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SNEI":
			value = SNEI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SLTI":
			value = SLTI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SGTI":
			value = SGTI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SLEI":
			value = SLEI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SGEI":
			value = SGEI;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LB":
			value = LB;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LH":
			value = LH;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LW":
			value = LW;
			type = InstructionType.IMMEDIATE;
			break;
		case "LBU":
			value = LBU;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LHU":
			value = LHU;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LF":
			value = LF;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "LD":
			value = LD;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SB":
			value = SB;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SH":
			value = SH;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SW":
			value = SW;
			type = InstructionType.IMMEDIATE;
			break;
		case "SF":
			value = SF;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		case "SD":
			value = SD;
			type = InstructionType.IMMEDIATE;
			notImplemented(opcode);
			break;
		default:
			try {
				value = SPECIAL;
				type = InstructionType.R_INT;
				fcode = new Functioncode(opcode);
			} catch (AssemblyException e) {
				value = FPARITH;
				type = InstructionType.R_FP;
				fpFcode = new FloatingPointFunctioncode(opcode);
			}
			break;
		}
	}

	public byte getValue() {
		return value;
	}

	public Functioncode getFunctioncode() {
		return fcode;
	}
	
	public FloatingPointFunctioncode getFPFunctioncode(){
		return fpFcode;
	}

	public InstructionType getType() {
		return type;
	}

	private void notImplemented(String opcode) {
		System.err.println("Instruction: " + opcode
				+ " is not implemented in DLX_MULTI_CYCLE!");
	}

}
