![License GPLv3](https://go-shields.herokuapp.com/License-GPLv3-green.png)

DLXAssembler is a assembly language compiler for [DLX-MC](https://gitlab.com/bschaefer/DLX-MC). For further information refer to that repository.